#  Task 7 : Maven; cloud deployment of App with Ansible

## Ansible

Ansible is an open-source automation tool, or platform, used for tasks such as configuration management, application deployment, intraservice orchestration and provisioning.

### Ansible playbook

The playbook is the core component of any Ansible configuration. An Ansible playbook contains one or multiple plays, each of which define the work to be done for a configuration on a managed server.

### Implementing Ansible

Ansible can only be installed in Unix type machines so, we will have to use a virtual machine to  be able to execute Ansible.

In this case I choose to create a virtual machine in **Google Cloud** that will contain both Jenkins and Ansible.

This virtual machine will be using Ubuntu 20.04 LTS Minimal.

### Structural changes to the project 

First we need to add the maven wrapper to our project. In order to do that we should run the following command inside maven terminal in our project:

```
mvn -N io.takari:maven:wrapper
```

Now, let's add a pluggin to our _POM.xml_ to generate a _.war_ file instead of the _.jar_ file:

```
<!-- Build an executable WAR -->
            <plugin>
                <artifactId>maven-war-plugin</artifactId>
                <configuration>
                    <attachClasses>true</attachClasses>
                    <webResources>
                        <resource>
                            <directory>src/main/java</directory>
                            <filtering>true</filtering>
                        </resource>
                    </webResources>
                </configuration>
            </plugin>
```

Now, to see if the pluggin is working let's run at project root the following command:

```
./mvnw package -Dmaven.test.skip=true
```

That will assemble our application without running test and the final _.war_ file will be created at /target/ folder.

### Running Jenkins on AWS (failed due to memory limitations of free tier VM)

To run Jenkins on AWS just follow this [tutorial](https://www.jenkins.io/doc/tutorials/tutorial-for-installing-jenkins-on-AWS/#Create%20a%20key%20pair%20using%20Amazon%20EC2).

After you sign in your AWS cloud, make sure you have git installed. You can do it by running:

```
sudo yum install git -y
```

In terms of configurations, that would be it.

Then you need to get your key pair credentials and configure EC2 cloud on Jenkins.

Finally you can proceed to deploy your **Jenkins** pipeline.

Unfortunatelly, since EC2's free tier only grants us **t2.tiny** virtual machine for free, and since it has only 1GB of RAM,
I had to say goodbye to this cloud and search for other, since...

![image](./prtsc/aws_my_jenkins_instance_time_to_say_goodbye.png)

... my current pipeline was constantly failing while assembling the application, even though sometimes it did pass, and when I went to look at the console output...

![image](./prtsc/aws_my_jenkins_instance_time_to_say_goodbye2.png)

... I know that was it.

1GB of RAM is not enough.

Therefore, **t2.tiny** instance of EC2 was not enough.

### Running Jenkins on Google Cloud

To run Jenkins on Google Cloud you need to create a virtual machine in there.

For this work assignment, I choose the following virtual machine:

![image](./prtsc/googlecloud_criar_instancia_promissor.png)

As you can see, this one has 4GB of RAM and runs on Ubuntu 20.04 LTS Minimal.

Then we need to install **JDK** for running **Jenkins**:

```shell
sudo apt update

sudo apt search openjdk

sudo apt install openjdk-11-jdk

sudo apt install openjdk-11-jdk 
```

You can then check your java version:

```shell
java -version 
```

 And **npm** and **yarn** to be able to build the frontend components:

```shell
sudo apt-get install npm
```

![image](./prtsc/googlecloud_instalar_npm.png)

```shell
sudo npm --global install yarn
```

![image](./prtsc/googlecloud_instalar_yarn.png)

And finally **sshpass** to be able to login to other machines by ssh:

```shell
sudo apt-get install sshpass
```

![image](./prtsc/googlecloud_install_sshpass.png)

Now we need to install jenkins:

```shell
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -

sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \
    /etc/apt/sources.list.d/jenkins.list'
	
sudo apt-get update

sudo apt-get install jenkins
```

Now we need to make **Jenkins** run on port **8081** because other apps may be using port **8080**.

So we need to follow the following steps provided by **Google Cloud**:

![image](./prtsc/googlecloud_passos_para_permitir_porta_8081.png)

Then we restart **Jenkins**:

```shell
sudo systemctl restart jenkins

sudo systemctl status jenkins
```

![image](./prtsc/googlecloud_restart_jenkins.png)

We can now go ahead and unlock **Jenkins**:

![image](./prtsc/googlecloud_unlock_jenkins.png)

As we can see, we need to get the password. We can do that by running:

```shell
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
```

This will print the initial admin password in our console.

![image](./prtsc/googlecloud_sudo_cat_password_jenkins.png)

Now, just like in the previous class assignments, we proceed to configure **Jenkins** until we finish it.

Then we will be granted this Jenkins's instance configuration URL:

![image](./prtsc/googlecloud_jenkins_url.png)

Now we can run our **Jenkins** and hope that 4GB of RAM are enough.

![image](./prtsc/googlecloud_jenkins_url.png)

And it most certainly was.

![image](./prtsc/jenkins_success_build.png)

As we can see our website is fully assembled, tested, documented, archived and deployed (more on that later).


### Running PostgreSQL on AWS

Now, I wouldn't give up on AWS so quickly, would I?

Free tier AWS could not provide us with a fully working environment capable of dealing with our CI/CD infrastructure,
but it surelly can help us out with out database provision.

First, we need to go to our **Console** then select **RDS**.

In there we create our database provision.

First we select our engine options:

![image](./prtsc/aws_database_creation.png)

Now we can follow this [tutorial](https://adamtheautomator.com/rds-postgres/)

After that we can see our database is up and running. It can take a few minutes for it to start though:

![image](./prtsc/aws_database_existing.png)

It needs to be said that this database has a storage capacity of 20GB and it's easily expandable. Not that we will ever need it, but it's a "nice to have".

Don't forget to change your security group to accept inbound communications from anywhere:

![image](./prtsc/aws_database_change_security_group.png)

Now we need to change the **application.properties** of our project to use our PostgreSQL database.

In my case I chose to make a new file, called **application-prod.properties**. This will connect our application to PostgreSQL and be used in production.

On the other side, **application-test.properties** will connect our application to H2 and be used in testing.


**application-prod.properties** database-related configurations:
```
spring.jpa.database=POSTGRESQL
spring.datasource.platform=postgres
spring.datasource.url=jdbc:postgresql://database-devops.ccew5up7x8eh.us-east-2.rds.amazonaws.com:5432/devops
spring.datasource.username=devops
spring.datasource.password=<REDACTED>
spring.jpa.show-sql=true
spring.jpa.generate-ddl=true
spring.jpa.hibernate.ddl-auto=create-drop
spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true
```

**application-test.properties** database-related configurations:
```
spring.jpa.show-sql=true
spring.h2.console.enabled=true
spring.datasource.url=jdbc:h2:mem:testdb2;DB_CLOSE_ON_EXIT=FALSE
spring.h2.console.path=/h2-console
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
```

After we run our application in production we can see our database tables filled with data.

![image](./prtsc/aws_database_postgresql_filled.png)

_Note: To manage and monitor our **PostgreSQL** database, we can use **pgAdmin4** application._

### Jenkinsfile

Now, first of all, our **Jenkins** will need the following plugins for this assignment:

- HTML Publisher;
- Javadoc;
- JaCoCo;
- Pipeline Maven Integration;
- Code Coverage API;
- Ansible.

Now for out actual **Jenkinsfile**, here's it's content:

```shell
pipeline {
    agent any
	
	environment {
        MAVEN_OPTS = '-Xmx512m -XX:MaxPermSize=128m'
        APP_PROFILE = 'prod'
    }

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'bitbucket_credentials', url: 'https://RicardoNunes230@bitbucket.org/RicardoNunes230/switch_2020_g1.git'
            }
        }

        stage('Assemble Backend') {
            steps {
                echo 'Assembling...'
                sh 'chmod +x ./mvnw'
                script {
                    if(isUnix() == true) {
                        sh './mvnw clean package -DskipTests'
                    } else {
                        bat './mvnw clean package -DskipTests'
                    }
                }
            }
        }
		
        stage('Javadoc') {
            steps{
                echo 'Javadoc'
                script{
                    if (isUnix() == true) {
                        sh './mvnw javadoc:javadoc'
                    } else {
                        bat './mvnw javadoc:javadoc'
                    }
                }
                publishHTML (target: [
                    keepAll: true,
                    reportDir: 'target/site/apidocs/',
                    reportFiles: 'index.html',
                    reportName: 'Javadoc'
                  ])
            }
        }

        stage('Test') {
            steps {
                echo 'Testing...'
                script{
                    if (isUnix() == true) {
                        sh './mvnw test jacoco:report -Dspring.profiles.active=test'
                    } else {
                        bat './mvnw test jacoco:report -Dspring.profiles.active=test'
                    }
                }
                echo 'Publishing test results...'
                echo 'Publishing code coverage...'
                junit skipPublishingChecks: true, testResults: 'target/surefire-reports/**/*.xml'
                publishCoverage adapters: [jacocoAdapter('target/site/jacoco/jacoco.xml')]
            }
        }

        stage('Install Frontend Dependencies') {
            steps {
                dir('src/main/ui'){
                    script {
                        if(isUnix() == true) {
                            sh 'yarn install --ignore-engines'
                        } else {
                            bat 'yarn install --ignore-engines'
                        }
                    }
                }
            }
        }
		
        stage('Assemble Frontend') {
            steps{
                echo 'Assemble Frontend'
                dir('src/main/ui'){
                    script {
                        if(isUnix() == true) {
                            sh 'yarn; CI=false yarn run build '
                        } else {
                            bat 'yarn; CI=false yarn run build'
                        }
                    }
                }
            }
        }

        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts artifacts: 'target/*.war'
                dir('src/main/ui'){
                    archiveArtifacts artifacts: '**/build/*'
                }
            }
        }

    
        stage('Deploying with Ansible') {
            steps {
                echo 'Deploying with Ansible...'
				
				dir('devops/7'){
					echo 'Deploying backend to DEI Cloud...'
				    sh 'env ANSIBLE_CONFIG=ansible.cfg'
					ansiblePlaybook disableHostKeyChecking: true, inventory: 'hosts', playbook: 'backend-playbook.yml'
					echo 'Deploying frontend to DEI Cloud...'
					ansiblePlaybook disableHostKeyChecking: true, inventory: 'hosts', playbook: 'frontend-playbook.yml'
                }
            }
        }
    }
}
```

### DEI Cloud

As you can see in the last stage of the **Jenkinsfile**, we are deploying our webapp to DEI cloud.

For that we use the following virtual machines:

![image](./prtsc/dei_my_virtual_servers_and_templates.png)

More information on my **"Frontend virtual machine"**:

![image](./prtsc/dei_frontend_vm.png)

More information on my **"Backend virtual machine"**:

![image](./prtsc/dei_backend_vm.png)

So we can see that:
- Frontend lies in http://vs217.dei.isep.ipp.pt
- Backend lies in http://vs457.dei.isep.ipp.pt

And our public HTTP access is:
- Frontend: http://vsgate-http.dei.isep.ipp.pt:10217
- Backend: http://vsgate-http.dei.isep.ipp.pt:10457

Moreover, our **frontend** is port-forwarded to port **3000** while our **backend** is port-forwarded to port **8080**.

### Details on Ansible Integration with Jenkins

As you can see in the last stage of the **Jenkinsfile**, I am using two **Ansible playbooks**, one for backend and another for frontend deployment and configuration.

My **backend-playbook.yml** content:

```yaml
---
- name: Deploy backend
  hosts: backend
  become: yes
  tasks:
    - name: Update apt cache
      apt:
        update_cache: yes
    - name: Install OpenJDK
      apt:
        name: openjdk-11-jre-headless
        state: present
    - name: Install Tomcat9
      apt:
        name: "{{ packages }}"
        state: latest
      vars:
        packages:
          - tomcat9
          - tomcat9-examples
          - tomcat9-docs
    - name: Make sure a service is running
      ansible.builtin.systemd:
        state: started
        enabled: true
        name: tomcat9
    - name: Stop Tomcat server
      service:
        name: tomcat9
        state: stopped
    - name: Delete remote war file
      file: path=/var/lib/tomcat9/webapps/project-1.0-SNAPSHOT.war state=absent
    - name: Deploy WAR file
      copy:
        src: ../../target/project-1.0-SNAPSHOT.war
        dest: /var/lib/tomcat9/webapps
    - name: Start Tomcat server
      service:
        name: tomcat9
        state: started
```

As you can see, it installs **JDK** and **Tomcat9** in my backend virtual machine.

Then it replaces the previous **war file** of our application, if there is one, and replaces it with the new **war file** assembled previously in the **Jenkins** pipeline, 
during stage **'Assemble Backend'**.

It then proceeds to start the **Tomcat** server once again.

My **frontend-playbook.yml** content:

```yaml
---
- name: Update Frontend
  hosts: frontend
  become: yes
  tasks:
    - name: Update and upgrade apt packages
      apt:
        upgrade: yes
        update_cache: yes
        cache_valid_time: 86400 #One day

    - name: Install apache2
      apt: name=apache2 state=present

    - name: Start Apache server
      service:
        name: apache2
        state: started
        enabled: true

    - name: Deploy Frontend
      copy:
        src: ../../src/main/ui/build/
        dest: /var/www/html/

    - name: Restart Apache
      service:
        name: apache2
        state: restarted
```

As you can see, first it upgrades and updates the virtual machine's cache for one day.

Then, it proceeds to install **Apache2** and start the **Apache** server.

After than, the previously assembled frontend components created in the **Jenkins** pipeline during stage **'Assemble Frontend'** are copied to the server's html directory.

Finally, **Apache** service is restarted in order to assure that the web application is updated.

Now, for the **ansible.cfg** configuration file:

```shell
[defaults]
inventory = ./hosts
remote_user = root
host_key_checking = false
[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s
```

Here we set the **inventory's path**, the **default remote user** to connect to other machines, we set **host key checking** to false to prevent 
unwanted and unfunded errors while connecting to our own virtual machines and we also set our default **ssh arguments**.

Finally, in our **hosts** file for inventory:

```shell
[otherservers]
backend ansible_ssh_host=vsgate-ssh.dei.isep.ipp.pt ansible_ssh_port=10457 ansible_ssh_user=root ansible_ssh_pass=VbLcA4zidMHR
frontend ansible_ssh_host=vsgate-ssh.dei.isep.ipp.pt ansible_ssh_port=10217 ansible_ssh_user=root ansible_ssh_pass=egnVrRGsjflk
```

Here we set both our backend and frontend ansible configurations and assign them to "otherservers" group.

For **backend** configurations:
- Host: vsgate-ssh.dei.isep.ipp.pt
- Port: 10457
- SSH user: root
- Password: VbLcA4zidMHR

For **frontend** configurations:
- Host: vsgate-ssh.dei.isep.ipp.pt
- Port: 10217
- SSH user: root
- Password: egnVrRGsjflk

### Final notes

Most unfortunatelly, while I tried my best to use the widest range of technologies that were available to me in order to get the maximum experience and 
practice from this final class assignment/project, I was not able to fully deploy the website, even though I am at the moment almost 4 hours late 
to deliver both this report and the class assignment itself.

There is currently one error that is disabling my frontend to reach my backend, which is:

![image](./prtsc/error.png)

When I try to login, the error displayed on the console at the right side of the printscreen appears.

I've been working on that error for the past 7-8 hours, with the help of:

- **Filipa Borges, number 1201785, from Group 1**
- **Ricardo Nogueira, number 1201780, from Group 3**
- **Filipa Santos, number 1201763, from Group 4**
- **Luís Rodrigues, number 1201769, from Group 4**
- **Pedro Santos, number 1201776, from Group 4**

And even with all this help, we could not solve this error. Therefore, the website is not fully working, although it is deployed.

What I can say is, to everyone that tried to help solve this problem, a big thank you for all your effort. And I hope to get this fixed soon.