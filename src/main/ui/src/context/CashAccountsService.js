import axios from 'axios';

export function getCashAccounts(userId){
    return axios.get(`http://vs457.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT/members/${userId}/cashAccounts`, {
        })
};

export function getMembers(userId, token){
    return axios.get(`http://vs457.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT/families/${userId}/membersAccounts`, {
        headers: {
            'Authorization': `Bearer ${token}`
        }})
};

export function transferMoney(state, originAccountId) {
    return axios.post(`http://vs457.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT/transactions/${originAccountId}/transfers`, state)
};

export function registerPayment(state, originAccountId) {
    return axios.post(`http://vs457.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT/transactions/${originAccountId}/payments`, state)
};

export function getFamilyCategories(familyId){
    return axios.get(`http://vs457.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT/categories/all/list/${familyId}`, {
    })
};

export function getCategoriesList(){
    return axios.get(`http://vs457.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT/categories/standard/list`, {
    })
};

