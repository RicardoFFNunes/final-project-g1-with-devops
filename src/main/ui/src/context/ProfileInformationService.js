import axios from "axios";

export const appURI = "http://vs457.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT";

export function fetchUserProfile(success, failure, id) {
  fetch(`${appURI}/members/${id}`)
    .then((result) => {
      if (result.ok) {
        result
          .json()
          .then((result) => success(result))
          .catch((error) => failure(error));
      } else {
        result
          .text()
          .then((error) => Promise.reject(error))
          .catch((error) => failure(error));
      }
    })
    .catch((error) => failure(error.message));

  // .then((result) => result.json())
  // .then((result) => success(result))
  // .catch((error) => failure(error.message));
}

export function deleteOtherEmail(mainEmail, otherEmail, token) {
  return axios.delete(`http://vs457.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT/members/${mainEmail}/emails/${otherEmail}`, {
    headers: {
      'Authorization': `Bearer ${token}`
    }
  })
}
