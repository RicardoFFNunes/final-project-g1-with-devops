import axios from 'axios';

export function getStandardCategories(token){
    return axios.get(`http://vs457.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT/categories/standard/tree`, {
        headers: {
            'Authorization': `Bearer ${token}`
        }})
}