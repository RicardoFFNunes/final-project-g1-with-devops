import axios from 'axios';

export function createFamilyAndAdministrator(data, token) {

    try {
        const addFamily = axios.post(`http://vs457.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT/families`, data, {
            headers: {
                'Authorization': `Bearer ${token}`
            }})
        return addFamily;
    } catch (error) {
        throw new Error(error);
    }
};

export function createFamilyMember(data, familyId, token) {
    try {
        const addMember = axios.post(`http://vs457.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT/families/${familyId}/members`, data, {
            headers: {
                'Authorization': `Bearer ${token}`
            }})
        return addMember;
    } catch (error) {
        throw new Error(error);
    }
}
export function getFamilyMembers(familyId, token){
    return axios.get(`http://vs457.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT/families/${familyId}/members`, {
        headers: {
            'Authorization': `Bearer ${token}`
        }})
};