import axios from 'axios';

export function createPersonalCashAccount(state, userId) {
    return axios.post(`http://vs457.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT/accounts/cash/${userId}`, state)
};

export function createBankAccount(state, userId) {
    return axios.post(`http://vs457.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT/accounts/${userId}`, state)
};